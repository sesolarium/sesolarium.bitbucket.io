# Letzte Änderungen:
## 2024-02-12
* Eingetragene Guthaben zählen mit zum verfügbaren Budget
* Nicht verfallene Budgets werden zum Guthaben addiert
## 2023-10-29
* Eine Termindauer von einer Minute über einem 5-Minuten-Intervall wird abgerundet
## 2023-07-12
* Die Dauer von Terminen wird auf 5-Minuten-Intervalle aufgerundet
## 2023-07-09
* Startzeiten werden nicht mehr auf 5-Minuten-Intervalle gerundet
* Mindestdauer von Terminen ist 5 Minuten statt 15 Minuten
* Split von Terminen mit einer Dauer von 5 oder 10 Minuten ist möglich
## 2023-06-11
* Der Kostenvoranschlag zeigt die Preise ab 05/23 an (2,98 für 5 min)
## 2023-03-23
* Die 30-Minuten-Buchungsregel wird bei kürzeren Terminen nicht angewendet
## 2022-02-12
* Es sind bis zu 12 Splitregeln möglich
## 2022-02-06
* Neue Splitregel: 30 min und ½ Anfahrt
* Das Datum der ersten Leistung kann in Rechnungsvorlagen verwendet werden
## 2021-12-20
* Die Statistik behandelt Protokolle mit Split korrekt
* Verbesserte Geschwindigkeit beim Arbeiten mit grossen Protokollen
## 2021-02-21
* Die Höhe des Investitionskostenzuschlags kann frei gewählt werden
## 2021-01-24
* Splitbuchung: Ab 2021 laufen Rechnungsummern pro Rechnungsvorlage
* Anzeige des Dateinamens im Fenstertitel
## 2021-01-17
* Splitbuchung: Fehler bei Anrechnung von halben Anfahrten auf Budgets behoben
* Rechnungsausgangsbuch: Getrennte Spalten für Kundenname und Rechnungsempfänger 
* Rechnungsausgangsbuch: Bindestrich statt Leerzeichen als Trenner für Rechnungsnummern 
* Buchungsregeln: In Schlagwörtern können Sonderzeichen (z.B. Raute) verwendet werden
## 2020-12-26
* Im Rechnungsausgangsbuch werden Auslagen nach Typ aufgeschlüsselt
## 2020-12-20
* Pro Buchungsregel kann ein eigener Text für Service-Leistungen vergeben werden
## 2020-12-12
* Buchungsregeln: Anfahrten können gesplittet werden
* Buchungsregeln: Der zweite Split kann einer weiteren Buchungsregel folgen
* Auto-Vervollständigung in der Zeichen-Zeile
* Zeilen in den Voreinstellungen können über das Kontextmenü gelöscht werden
## 2020-11-20
* Der Kostenvoranschlagsdialog wurde leicht überarbeitet
## 2020-11-19
* Bis zu sechs Vorlagen können hinterlegt werden
* Ein Kostenvoranschlag kann über eine Vorlage geöffnet werden
* Fehler behoben: Investitionskostenzuschlag wird in der Privatrechnung aufgelistet
## 2020-11-18
* Kostenvoranschlag basiert auf der letzten erstellten Rechnung
* Fehler behoben: Investitionskostenzuschlag wird immer der Privatrechnung zugeordnet
## 2020-11-15
* Neu: Kostenvoranschlag (einfache Version)
* Splitbuchung: Split auf Privatrechnung wird korrekt zugeordnet
## 2020-11-14
* Splitbuchung: Fehler bei Buchungsregeln (Nur anwenden bei/Nicht anwenden bei) behoben
* Splitbuchung: Buchungsregeln erlauben unbegrenztes Budget
## 2020-11-13
* Investitionskostenzuschlag geändert auf 9,08 %
## 2020-11-12
* Neue Voreinstellung: Investitionskostenzuschlag 9 %
* Geänderte Tastenkürzel für 15-Min-Abspalten
## 2020-11-01
* Splitbuchung pro Rechnung statt pro Monat, um Sonderfälle besser zu ermöglichen
* Anzeige des Preises pro Stunde statt pro 30 Minnuten
## 2020-10-18
* Splitbuchung: Termine können komplett auf ein Budget gebucht werden
* Splitbuchung: Regeln lassen sich auf ein Schlagwort eingrenzen
## 2020-10-08
* Geänderte Rundung bei Dauer: Minimum 15 min, danach auf 5 min auf-/abrunden
* Rechnungsausgangsbuch: Verkürzte Rechnungsnummern, Name, Vorname statt Vorname Name
## 2020-09-20
* Reihenfolge beim 15-Min-Abspalten geändert: 15 Minuten stehen oben
* Neuer Menübefehl: 15 min + ½ Anfahrt abspalten
## 2020-09-14
* Rechnungsausgangsbuch: Zwei neue Spalten für Leistungen und Auslagen
* Verbesserung der Benutzerfreundlichkeit beim Schließen von Protokollen
* Neue Adresse für Software-Updates (bitbucket.org/sesolarium)
## 2020-08-29
* Splitbuchung: Einträge ohne Uhrzeit werden nicht gesplittet
## 2020-08-22
* Fehler bei Splitbuchung behoben: Mehrere Budgets werden vollständig verteilt
## 2020-08-16
* Splitbuchung: Besserer Verbrauch von mehreren Budgets
## 2020-08-10
* Fehler behoben: Abspalten von 15 min wird nicht mehr fälschlich ausgegraut 
* Halbe Anfahrt ist möglich
## 2020-07-17
* Fehler behoben: Splitrechnungen enthalten alle Zeilen
* Geändertes Tastenkürzel: Woche einfügen mit Strg+7
## 2020-06-22
* Rechnungsausgangsbuch (CSV-Export)
* Statistik nach Jahren, Monaten oder Wochen
## 2020-06-17
* Statistik in Jahren statt Monaten
* Tastenkürzel zum Schließen einer Datei
## 2020-05-03
* Neuer Menüpunkt: 15 min abspalten
## 2020-03-31
* In Rechungsvorlagen ist der Zugriff auf die Kunden-ID möglich
## 2020-03-26
* Splitbuchung: Einträge ohne Anfahrt werden nicht gesplittet
* Buchungsregeln lassen sich mit einem Schlüsselwort ausser Kraft setzen
## 2020-03-20
* Splitbuchung: Feiertagszuschlag und Sonderpreis erscheinen korrekt auf der Rechnung
* Einfachere Dateinamen für Rechnungen
## 2020-03-13
* Zweistellige Rechnungsnummer ab 2020
## 2020-03-12
* Fehler behoben: Der Menüpunkt "Löschen" wird korrekt ausgegraut
## 2020-03-06
* Im Protokoll eingegebene Zeiten werden automatisch gerundet
* Splitbuchung unterstützt fünf Vorlagen
* Splitkonfiguration: kleinere Korrekturen
## 2020-03-02
* Einträge können mit "Krank" markiert werden, um Split zu unterbinden
## 2020-02-24
* Splitbuchung: Mehrere Budgets werden berücksichtigt
* Fehlerkorrektur bei Splitbuchung: Privatrechnung enthält immer Vielfache von 15 min
* Kürzerer Sicherheitscode beim Stornieren von bereits erstellten Rechnungen
## 2020-02-07
* Splitbuchung: Individuelle Rechnung pro Split
## 2020-01-27
* Splitbuchung: Budget wird auf maximal viele Anfahrten verteilt
## 2020-01-21
* Fehlerkorrekturen bei der Anzeige des Gesamtbetrags und beim Öffnen der Rechnung
## 2020-01-19
* Splitbuchung: Fehlerkorrekturen, verbesserte Anzeige des Budgets im Protokoll
## 2020-01-17
* Splitkonfiguration Vorabversion: Es werden noch nicht alle Einstellungen angezogen
* Splitbuchung Vorabversion: Das erste Budget wird schnellstmöglich verbraucht
* Das Erstellen gesplitteter Rechnungen ist noch nicht möglich
## 2018-10-28
* Fehler beim Markieren von Protokolleinträgen behoben
## 2018-10-24
* Anfahrten ohne Preis erscheinen im Protokoll als Anzahl
* Bereits erstellte Rechnungen können widerrufen werden
* Spezialpreise wirken sich nicht mehr auf die Anfahrtskosten aus
## 2018-09-06
* Fehler bei der Berechnung des Gesamtbetrags ab Juni 2018 behoben
## 2018-07-22
* Storno: Anfahrten mit Minus können im Protokoll eingetragen werden
## 2018-06-26
* Neue Berechnung ab Juni 2018: Kein Feiertagszuschlag auf Anfahrten
* Das Protokoll zeigt die Kosten der Anfahrten an
* Das Protokoll zeigt die Zeitkosten statt der Gesamtkosten
## 2018-06-17
* Auto-Vervollständigung im Service-Protokoll
* Optische Verbesserungen für hochauflösende Bildschirme
* Verlauf der Updates kann über das Hilfe-Menü eingesehen werden
## 2018-06-14
* Beschriftung geändert: Anfahrten statt Kilometer
* Beträge für Zeiten und Anfahrten können in Rechnungen separat ausgewiesen werden
## 2018-06-10
* Gesamtkilometer werden im Protokoll angezeigt
* Gesamtzeit und -kilometer können in Rechnungen ausgewiesen werden
## 2018-06-07
* Rechnungen können erstellt werden, ohne sie sofort anzuzeigen
## 2018-04-19
* Die Statistik zeigt die Gesamtsumme der angezeigten Daten an
## 2018-03-19
* Die Rechnungsliste zeigt Rechnungen mit Auslagen korrekt an
## 2017-04-06
* Fehler beim Export von Adressdaten behoben
## 2017-04-02
* Setup erkennt Java zuverlässiger
* Statistiken können als CSV angezeigt werden
## 2017-01-17
* Geöffneten Dateien lassen sich einfacher durchblättern
* Beim Schliessen wird die nachfolgende offene Datei ausgewählt
## 2016-04-24
* Statistiken zeigen bis zu 15 Monate an
## 2015-12-25
* CSV-Export für Adressdaten
## 2015-12-24
* vCard-Export für Adressdaten
## 2015-08-15
* Fehler behoben: Auslagen vervielfachen sich nicht mehr
## 2015-02-23
* Anzeige der Gesamtdauer im Protokoll
## 2013-06-13
* Neue Statistiken (Pauschalen, Anzahl unentgeltlicher Leistungen)
## 2013-03-14
* Rechnungsvorlagen können Platzhalter mit Zahlen auswerten
## 2013-02-19
* Rechnung an wählbarem Datum (Bearbeiten - Zwischenrechnung einfügen)
## 2013-02-03
* Dauer über 24 h möglich
* Negative Dauer (Storno) möglich
## 2013-01-27
* Der automatische Sonntagszuschlag kann in den Voreinstellungen abgeschaltet werden
## 2012-08-29
* Fehler beim Anklicken des Symbols für Rechnungserstellung behoben
## 2012-08-28
* Dauer kann in Rechnungen angezeigt werden
## 2012-08-26
* Neue Spalte im Protokoll: Dauer
## 2012-08-14
* SeSoLarium-Dateien lassen sich unter Windows XP wieder durch Anklicken öffnen
## 2012-07-29
* Statistik über Stunden (gesamt/unentgeltlich) und Kilometer
* Einfügen weiterer Zeilen in Voreinstellung per Klick möglich
## 2011-10-30
* Statistik verarbeitet auch Protokolle ohne eingetragenes Datum
## 2011-07-10
* Wöchentliche Abrechnung möglich
## 2011-04-22
* Monatsstatistik (Datei - Daten auswerten)
## 2010-12-19
* Neues Protokoll aus bestehender Vorlage erstellen
## 2010-12-08
* Ganze Woche einfügen im Bearbeiten-Menü
* Hervorhebungen in Notizen
## 2010-08-28
* Abgerechnete Leistungen: Anzahl sichtbarer Rechnungen wählbar
* Geschwindigkeitsverbesserungen bei großen Protokollen
## 2010-01-18
* Absturz beim Einfügen eines neuen Tags nach manueller Abrechnung behoben
* 14-tägig umfasst jetzt 3 Wochen bei Jahreswechseln mit Kalenderwoche 53
## 2009-11-22
* Fenstergröße bleibt über Neustart erhalten
* Spalten: Service-Leistungen schmaler, Zeichen breiter
## 2009-10-22
* Manuelle Abrechnung (über alle offenen Tage)
* Verbesserte Geschwindigkeit beim Ändern sehr langer Protokolle
## 2009-10-05
* Dynamische Spaltenbreite in Tabellen
## 2009-10-01
* Löschen mehrerer Zeilen